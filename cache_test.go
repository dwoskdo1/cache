package cache

import (
	"fmt"
	"log"
	"testing"
)

var i int = 0

func testGetMore(repopulateCount int) ([][]byte, error) {

	log.Printf("testPopulator:GetMore %d\n", repopulateCount)

	bs := make([][]byte, repopulateCount)
	for x := 0; x < repopulateCount; x++ {
		bs[x] = []byte(fmt.Sprintf("id-%d", i))
		i = i + 1
	}
	return bs, nil
}

func printem(bs [][]byte) {
	for _, b := range bs {
		log.Printf("%s\n", string(b))
	}
}

func TestCache(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}

	cache, err := NewCache("testCache", 5, testGetMore)

	if err != nil {
		t.Fail()
	}

	log.Println("trying first test for 1")
	s2, err2 := cache.Fetch(1)
	if err2 != nil {
		log.Println(err2)
		t.Fail()
	}
	if len(s2) != 1 {
		log.Printf("Asked for 1 id and got %d\n", len(s2))
		t.Fail()
	}
	printem(s2)

	log.Println("trying test for 5")
	s3, err3 := cache.Fetch(5)
	if len(s3) != 5 {
		log.Printf("Asked for 5 id and got %d\n", len(s3))
		t.Fail()
	}
	if err3 != nil {
		log.Println(err3)
		t.Fail()
	}
	printem(s3)

	log.Println("trying test for 20")
	s4, err4 := cache.Fetch(20)
	if len(s4) != 20 {
		log.Printf("Asked for 5 id and got %d\n", len(s4))
		t.Fail()
	}
	if err4 != nil {
		log.Println(err4)
		t.Fail()
	}
	printem(s4)
}
